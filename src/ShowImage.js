import React from 'react';
import {View, Dimensions, ImageBackground} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const ShowImage = props => {
  return (
    <View>
      <ImageBackground
        source={{uri: props.route.params.url}}
        style={{height: deviceHeight, width: deviceWidth}}
      />
    </View>
  );
};

export default ShowImage;
