import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  Text,
} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Home = props => {
  const [images, setimages] = useState([]);
  useEffect(() => {
    fetch(
      'https://api.unsplash.com/photos?client_id=896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043',
    )
      .then(response => response.json())
      .then(body => {
        const imagesData = body.map(el => el.urls.regular);
        setimages([...imagesData]);
      });
  }, []);

  return (
    <ScrollView>
      <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
        {images.length ? (
          images.map((image, index) => (
            <TouchableOpacity
              key={index}
              onPress={() =>
                props.navigation.navigate('ShowImage', {
                  url: image,
                })
              }>
              <Image
                source={{uri: image}}
                style={{
                  height: deviceHeight / 3,
                  width: deviceWidth / 3 - 6,
                  borderRadius: 10,
                  margin: 2,
                }}
              />
            </TouchableOpacity>
          ))
        ) : (
          <Text>Loading</Text>
        )}
      </View>
    </ScrollView>
  );
};

export default Home;
